# Shmup
A 10 minutes shoot them up experience, made with Unity, as a CNAM-ENJMIN one-month project.

## Required
A leaderboard and the scoring notion must exists along side of a scrolling mechanic.

## Members 
Mathieu RENARD      - Manager
Ervan SILVERT       - Programmer
Valentin LEGEAY     - Programmer
Alison PERNET       - Game Designer
Paul GASTON         - Game Designer
Thomas GARCIA       - Graphic Designer
Romain NAVAZO       - Graphic Designer
Robin RICHARD       - Sound Designer
Antoine DANIELOU    - Ergonomist

## Technology
We are using Unity (v3.0.1) as middleware.

## Bugs
None known.
